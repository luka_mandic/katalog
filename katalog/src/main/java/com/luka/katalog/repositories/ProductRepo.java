package com.luka.katalog.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.luka.katalog.models.Product;


public interface ProductRepo extends JpaRepository<Product, Integer>{
	
	List<Product> findByCategory_Id(Integer category_id);
	
	@Query(value = "SELECT COUNT(*) FROM product WHERE product.category  = ?1", nativeQuery = true)
	Integer numOfProductsPerCategory(Integer category);
}
