package com.luka.katalog.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import com.luka.katalog.models.Category;


public interface CategoryRepo extends JpaRepository<Category, Integer>{
	
}
