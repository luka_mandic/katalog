package com.luka.katalog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.luka.katalog.models.User;

public interface UserRepo extends JpaRepository<User, Integer>{
	
	public User findByUsername(String username);

}
