package com.luka.katalog.controllers;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.luka.katalog.models.Category;
import com.luka.katalog.models.Product;
import com.luka.katalog.repositories.CategoryRepo;
import com.luka.katalog.repositories.ProductRepo;

@Controller
public class ProductController {
	
	@Autowired
	ProductRepo productRepo;
	
	@Autowired
	CategoryRepo categoryRepo;
	
	@GetMapping("/products")	
    public ModelAndView getProducts() {
		ModelAndView mv = new ModelAndView("view-product");
		mv.addObject("products", productRepo.findAll());
		
		return mv;
    }
	
	
	@GetMapping("/products/{categoryId}")	
    public ModelAndView getProductsForCategory(@PathVariable Integer categoryId) {
		ModelAndView mv = new ModelAndView("view-product");
		mv.addObject("products", productRepo.findByCategory_Id(categoryId));
		
		return mv;
    }
	
	@GetMapping("/products/edit/{id}")
	public ModelAndView editCategory(@PathVariable int id) {
		Optional<Product> p = productRepo.findById(id);
		
		if(p.isEmpty()) {
			ModelAndView mv = new ModelAndView("notification");
			mv.addObject("errorMsg", "Ne postoji proizvod s id: " + id);
			return mv;
		}
		
		
		
		ModelAndView mv = new ModelAndView("edit-product");
		mv.addObject("product", p.get());	
		mv.addObject("categories", categoryRepo.findAll());
		return mv;		
	}
	
	@PostMapping("/products/edit/{id}")
	public String editProduct(@PathVariable Integer id, @RequestParam String name, Integer category,
			Double price, Model model) {
		
		Optional<Product> p = productRepo.findById(id);
		if(!p.isEmpty()) {
			if(categoryRepo.findById(category).isEmpty()) {
				model.addAttribute("errorMsg", "Ne postoji kategorija s id: " + category);
				return "notification";
			}
			Category c = categoryRepo.findById(category).get();
			
			p.get().setName(name);
			p.get().setCategory(c);
			p.get().setPrice(price);
			productRepo.save(p.get());
		}
		else {
			model.addAttribute("errorMsg", "Ne postoji proizvod s id: " + id);
			return "notification";
		}

		ModelAndView mv = new ModelAndView("view-product");
		mv.addObject("products", productRepo.findAll());
		return "redirect:/products";
	}
	
	
	@GetMapping("/products/edit")
	public ModelAndView editProduct() {
		ModelAndView mv = new ModelAndView("edit-product");
		mv.addObject("products", null);
		mv.addObject("categories", categoryRepo.findAll());
		return mv;		
	}
	
	@PostMapping("/products/edit")
	public String editProduct(@RequestParam String name, Integer category,
			Double price, Model model) {
		
		if(categoryRepo.findById(category).isEmpty()) {
			model.addAttribute("errorMsg", "Ne postoji kategorija s id: " + category);
			return "notification";
		}
		
		Category c = categoryRepo.findById(category).get();
		Product p = new Product(name, price, c);
		productRepo.save(p);
		
		return "redirect:/products";
	}
	
	@PostMapping("/products/delete/{id}")
	public String deleteProduct(@PathVariable int id){
		Optional<Product> p = productRepo.findById(id);
		
		if(p.isEmpty()) {
			ModelAndView mv = new ModelAndView("notification");
			mv.addObject("errorMsg", "Ne postoji proizvod s id: " + id);
			return "notification";
		}
		
		productRepo.delete(p.get());
		
		ModelAndView mv = new ModelAndView("view-product");
		mv.addObject("products", productRepo.findAll());
		return "redirect:/products";
	}
	
	@GetMapping("test/{id}")
	public ModelAndView test(@PathVariable Integer id) {
		ModelAndView mv = new ModelAndView("notification");
		
		for (Product p : productRepo.findByCategory_Id(id)) {
			System.out.println(p.getName());
		}
		
		mv.addObject("errorMsg", productRepo.numOfProductsPerCategory(id));
		
		return mv;
	}

}
