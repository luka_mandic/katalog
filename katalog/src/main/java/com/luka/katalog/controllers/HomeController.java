package com.luka.katalog.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
	
	@GetMapping("/home")	
    public ModelAndView getHome() {
		ModelAndView mv = new ModelAndView("home");
        return mv;
    }

}
