package com.luka.katalog.controllers;


import java.util.HashMap;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.luka.katalog.models.Category;
import com.luka.katalog.repositories.CategoryRepo;
import com.luka.katalog.repositories.ProductRepo;

@Controller
public class CategoryController {
		
	@Autowired
	CategoryRepo categoryRepo;
	
	@Autowired
	ProductRepo productRepo;
	
	
	@GetMapping("/categories")	
    public ModelAndView getCategories() {
		ModelAndView mv = new ModelAndView("view-category");
		mv.addObject("categories", categoryRepo.findAll());
		
		HashMap<Category, Integer> productsPerCategory = new HashMap<Category, Integer>();
		for (Category c : categoryRepo.findAll()) {
			productsPerCategory.put(c, productRepo.numOfProductsPerCategory(c.getId()));
		}
		mv.addObject("productsPerCategory", productsPerCategory);
		
        return mv;
    }
	
	
	@GetMapping("/categories/edit/{id}")
	public ModelAndView editCategory(@PathVariable int id) {
		Optional<Category> c = categoryRepo.findById(id);
		
		if(c.isEmpty()) {
			ModelAndView mv = new ModelAndView("notification");
			mv.addObject("errorMsg", "Ne postoji kategorija s id: " + id);
			return mv;
		}
		
		ModelAndView mv = new ModelAndView("edit-category");
		mv.addObject("category", c.get());	
		return mv;		
	}
	
	
	@GetMapping("/categories/edit")
	public ModelAndView editCategory() {
		ModelAndView mv = new ModelAndView("edit-category");
		mv.addObject("category", null);
		return mv;		
	}
	
	
	@PostMapping("/categories/edit/{id}")
	public String editCategory(@PathVariable Integer id, @RequestParam String name, Model model) {
		
		Optional<Category> c = categoryRepo.findById(id);
		if(!c.isEmpty()) {
			c.get().setName(name);
			categoryRepo.save(c.get());
		}
		else {
			model.addAttribute("errorMsg", "Ne postoji kategorija s id: " + id);
			return "notification";
		}

		ModelAndView mv = new ModelAndView("view-category");
		mv.addObject("categories", categoryRepo.findAll());
		return "redirect:/categories";
	}
	
	
	@PostMapping("/categories/edit")
	public String editCategory(@RequestParam String name) {
		Category c = new Category(name);
		categoryRepo.save(c);
		
		return "redirect:/categories";
	}
	
	
	@PostMapping("/categories/delete/{id}")
	public String deleteCategory(@PathVariable int id){
		Optional<Category> c = categoryRepo.findById(id);
		
		if(c.isEmpty()) {
			ModelAndView mv = new ModelAndView("notification");
			mv.addObject("errorMsg", "Ne postoji kategorija s id: " + id);
			return "notification";
		}
		
		categoryRepo.delete(c.get());
		
		ModelAndView mv = new ModelAndView("view-category");
		mv.addObject("categories", categoryRepo.findAll());
		return "redirect:/categories";
	}
	
	
}
