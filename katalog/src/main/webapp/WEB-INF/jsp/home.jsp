<style><%@include file="/WEB-INF/css/style.css"%></style>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div id="home">
<a class="home-link" href="${'/products'}">Proizvodi</a>
<a class="home-link" href="${'/categories'}">Kategorije</a>
</div>
<br><br><br>

<a class="home-link" href="${'/login'}">Admin login</a>
<br><br><br>

<sec:authorize access="isAuthenticated()">
   Hello admin!
</sec:authorize>