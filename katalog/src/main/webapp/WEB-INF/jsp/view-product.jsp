<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Proizvodi</title>
</head>
<style><%@include file="/WEB-INF/css/style.css"%></style>
<body>
	<sec:authorize access="isAuthenticated()">	
		<a href="${'/products/edit'}">Novi proizvod</a>
		<br>
		<br>
	</sec:authorize>

	<table>
	  <tr>
	    <th>Naziv</th>
	    <th>Cijena</th>
	    <th>Kategorija</th>
	    <sec:authorize access="isAuthenticated()">
		    <th></th>
		    <th></th>
	    </sec:authorize>
	  </tr>
		<c:forEach items="${products}" var="product">
			<tr>
				<td><c:out value="${product.getName()}"/></td>
				<td><c:out value="${product.getPrice()}"/></td>
				<td><c:out value="${product.getCategory().getName()}"/></td>
				<sec:authorize access="isAuthenticated()">
					<td><a href="${'/products/edit/'.concat(product.getId())}">Uredi</a></td>
				</sec:authorize>
				<sec:authorize access="isAuthenticated()">
					<td>
						<form action="${'/products/delete/'.concat(product.getId())}" method="POST">
						    <input type="submit" value="Izbriši" id="deleteButton"/>
						</form>
					</td>
				</sec:authorize>
			</tr>
		</c:forEach>
	</table>

</body>
</html>