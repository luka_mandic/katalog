<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Proizvodi</title>
</head>
<style><%@include file="/WEB-INF/css/style.css"%></style>
<body>
	<a href="${'/home/'}">Početna</a>
	<br><br>

	<c:if test="${product != null}"><form action="/products/edit/${product.id}" method="post" ></c:if>
	<c:if test="${product == null}"><form action="/products/edit" method="post" ></c:if>
		
	  <p>	
	  <label for="name">Naziv:</label>
	  <input type="text" id="name" name="name" value="${product.name}">
	  </p>
	  <br>
	  
	  <p>
	  <label for="name">Cijena:</label>
	  <input type="text" id="price" name="price" value="${product.price}" >
	  <br>
	  <br>
	  </p>

	   <p>
	   <label for="category">Kategorija:</label> 
	   <select name="category" id="category">
		   <c:forEach items="${categories}" var="category">
		   	<c:if test="${product.category.id == category.id}">
		   		<option selected value="${category.id}" label="${category.name}"/> 
		   	</c:if>
		   	<c:if test="${product.category.id != category.id}">
		   		<option value="${category.id}" label="${category.name}"/> 
		   	</c:if>
		   </c:forEach>  
       </select>
       </p>  
	  <br><br><br>
	  
	  <p>
	  <label></label>
	  <input type="submit" value="Spremi" id="submit">
	  </p>
	</form> 

</body>
</html>