<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Kategorije</title>
</head>
<style><%@include file="/WEB-INF/css/style.css"%></style>
<body>
	<a href="${'/home/'}">Početna</a>
	<br><br>
	
	<c:if test="${category != null}">
		<form action="/categories/edit/${category.id}" method="post">
	</c:if>
	
	<c:if test="${category == null}">
		<form action="/categories/edit" method="post">
	</c:if>
			<label for="name">Naziv:</label>
			<input type="text" id="name" name="name" value="${category.name}" >
			<br><br><br>
		  
		  	<input type="submit" value="Spremi">
		</form>

</body>
</html>