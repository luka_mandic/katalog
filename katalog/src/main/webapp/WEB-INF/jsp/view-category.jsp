<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Kategorije</title>
</head>
<style><%@include file="/WEB-INF/css/style.css"%></style>
<body>
	<sec:authorize access="isAuthenticated()">	
		<a href="${'/categories/edit'}">Nova kategorija</a>
		<br>
		<br>
	</sec:authorize>

	<table>
	  <tr>
	    <th>Naziv</th>
	    <sec:authorize access="isAuthenticated()">
		    <th></th>
		    <th></th>
	    </sec:authorize>
	    <th>Proizvodi</th>
	  </tr>
		<c:forEach items="${categories}" var="category">
			<tr>
				<td><c:out value="${category.getName()}"/> (${productsPerCategory.get(category)}) </td>

				<sec:authorize access="isAuthenticated()">
					<td>
						<a href="${'/categories/edit/'.concat(category.getId())}">Uredi</a>
					</td>
				</sec:authorize>
				
				<sec:authorize access="isAuthenticated()">
					<td>
						<form action="${'/categories/delete/'.concat(category.getId())}" method="POST">
						    <input type="submit" value="Izbriši" id="deleteButton"/>
						</form>
					</td>
				</sec:authorize>
				<td>
					<a href="${'/products/'.concat(category.getId())}">Proizvodi</a>
				</td>
			</tr>
		</c:forEach>
	</table>

</body>
</html>